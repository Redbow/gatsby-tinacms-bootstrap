import './src/assets/styles/style.css';

import React from 'react';
import GlobalContextProvider from './src/context/GlobalContextProvider';

// eslint-disable-next-line import/prefer-default-export
export const wrapRootElement = ({ element }) => (
  <GlobalContextProvider> {element} </GlobalContextProvider>
);
