/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: 'Burrichter',
    description: 'Website für Consulting-Unternehmen',
    author: 'Hendrik Burrichter',
  },
  pathPrefix: '/gatsby-tinacms-bootstrap',
  plugins: [
    'gatsby-plugin-postcss',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    'gatsby-transformer-remark',
    'gatsby-transformer-json',
    'gatsby-tinacms-json',
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-plugin-purgecss',
      options: {
        printRejected: false, // Print removed selectors and processed file names
        develop: false, // Enable while using `gatsby develop`
        tailwind: true,
        whitelist: ['bg-white, bg-gray-900'], // Don't remove this selector
      },
    },
    {
      resolve: 'gatsby-plugin-favicon',
      options: {
        logo: './static/favicon.png',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/data/`,
      },
    },
    {
      resolve: 'gatsby-plugin-tinacms',
      options: {
        sidebar: {
          hidden: process.env.NODE_ENV === 'production',
        },
        plugins: [
          'gatsby-tinacms-git',
          'gatsby-tinacms-json',
          'gatsby-tinacms-remark',
        ],
      },
    },
  ],
};
