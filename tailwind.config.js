const { fontFamily, colors } = require('tailwindcss/defaultTheme');

module.exports = {
  theme: {
    colors: {
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
    },
    extend: {
      fontFamily: {
        body: ['Helvetica', ...fontFamily.sans],
      },
      spacing: {
        'screen-75': '75vh',
      },
    },
  },
  variants: {},
  plugins: [],
};
