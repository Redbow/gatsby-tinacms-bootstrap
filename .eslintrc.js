module.exports = {
  plugins: [
    'react'
  ],
  root: true,
  env: {
    node: true,
    "browser": true
  },
  extends: [
    'airbnb-base',
    'plugin:react/recommended'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'max-len': ['warn',{
      'code': 150,
      'ignoreStrings': true,
      'ignoreUrls': true,
    }],
    'indent': ['warn', 2],
    'react/display-name': ['off'],
    'react/prop-types': ['off'],
    'class-methods-use-this': ['off'],
    'arrow-parens': ['error', 'as-needed'],
    'no-underscore-dangle': ['off'],
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaFeatures: {
      jsx: true
    }
  },
};
