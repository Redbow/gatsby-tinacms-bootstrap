import React, { useEffect } from 'react';
import { processToken } from '../utils/auth-service';
import { isBrowser } from '../utils/utils';

function findTokenInHash(hash) {
  const matchedResult = hash.match(/access_token=([^&]+)/);
  return matchedResult && matchedResult[1];
}

export default () => {
  const accessToken = isBrowser() && findTokenInHash(window.location.hash);

  useEffect(() => processToken(accessToken));

  return <h1>Redirecting back to CMS</h1>;
};
