import React from 'react';
import { Link, useStaticQuery, graphql } from 'gatsby';

import Layout from '../components/layout';

export default () => {
  const data = useStaticQuery(graphql`
    query BlogPosts {
      allMarkdownRemark(filter: {fields: {slug: {regex: "/^\/posts\/.*/"}}}) {
        edges {
          node {
            id
            excerpt
            fields {
              slug
            }
            frontmatter {
              title
              date(formatString: "DD MMMM, YYYY")
            }
          }
        }
      }
    }
  `);

  return (
    <Layout type="markdown">
      <section className="px-4 pt-12 text-center w-1/2 mx-auto">
        <h1>Blog Posts</h1>
        <ul className="text-left list-none mt-8">
          { data.allMarkdownRemark.edges.map(({ node }) => (
            <li className="mb-12" key={node.id}>
              <Link to={node.fields.slug}>
                <h4>{node.frontmatter.title}</h4>
                <h6 className="mt-0 mb-2"><small>{node.frontmatter.date}</small></h6>
                <p>{node.excerpt}</p>
              </Link>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  );
};
