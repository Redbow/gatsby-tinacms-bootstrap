import React from 'react';

import Layout from '../components/layout';

export default () => (
  <Layout type="markdown">
    <section style={{ padding: '120px 20px', margin: '0 auto', maxWidth: 1200 }}>
      <h1>This page could not be found.</h1>
    </section>
  </Layout>
);
