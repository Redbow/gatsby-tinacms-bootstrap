import React, { useState, useContext } from 'react';
import { navigate } from 'gatsby';

import { GlobalStateContext, GlobalDispatchContext } from '../context/GlobalContextProvider';
import { launchPopup, listenToMessageEvent } from '../utils/auth-service';
import Button from '../components/elements/button';

export default () => {
  const globalState = useContext(GlobalStateContext);
  const dispatch = useContext(GlobalDispatchContext);
  const [processState, setProcessState] = useState('');
  async function authenticateUsingOAuth() {
    const popup = launchPopup();

    const interval = setInterval(() => {
      if (popup.closed) {
        setProcessState('');
        clearInterval(interval);
      }
    }, 100);

    return new Promise((resolve, reject) => {
      listenToMessageEvent(resolve, reject);
    });
  }

  async function authenticate() {
    try {
      const response = await authenticateUsingOAuth();
      if (!response) {
        throw new Error('No authentication message received!');
      }
      return { response };
    } catch (e) {
      return { error: { message: e } };
    }
  }

  function handleLoginClick() {
    if (globalState.accessToken) {
      return;
    }
    setProcessState('popup');
    authenticate().then(({ response }) => {
      setProcessState('');
      dispatch({ type: 'SET_ACCESS_TOKEN', payload: response });
      navigate(globalState.redirectAfterLogin);
    });
  }

  return (
    <div className="container py-48 text-center m-auto">
      <h3 className="pb-8 leading-normal">Bitte klick auf den Button<br/> um dich einzuloggen!</h3>
      <Button
        isLoading={processState === 'popup'}
        onClick={() => handleLoginClick(globalState, dispatch, setProcessState)}>
        Login mit GitLab
      </Button>
      { processState === 'popup'
        ? <h6 className="mt-12">Benutze das Popup um dich einzuloggen.<br/>
          <small>Achte darauf, dass Popups für diese Webseite erlaubt sind.</small>
        </h6>
        : null }
    </div>
  );
};
