import React, { useReducer, createContext } from 'react';

export const GlobalStateContext = createContext();
export const GlobalDispatchContext = createContext();

const initialState = {
  accessToken: null,
  redirectAfterLogin: '/',
};

const reducer = (state, action) => {
  switch (action.type) {
  case 'SET_ACCESS_TOKEN':
    return {
      ...state,
      accessToken: action.payload,
    };
  case 'SET_REDIRECT_AFTER_LOGIN':
    return {
      ...state,
      redirectAfterLogin: action.payload,
    };
  default:
    throw new Error('No valid action type.');
  }
};

const GlobalContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <GlobalStateContext.Provider value={state}>
      <GlobalDispatchContext.Provider value={dispatch}>
        { children }
      </GlobalDispatchContext.Provider>
    </GlobalStateContext.Provider>
  );
};

export default GlobalContextProvider;
