import { JsonCreatorPlugin } from 'gatsby-tinacms-json';
import { RemarkCreatorPlugin } from 'gatsby-tinacms-remark';
import slugify from 'slugify';

export const CreatePagePlugin = new JsonCreatorPlugin({
  label: 'Neue Seite',
  filename: form => `data/pages/${slugify(form.title).toLowerCase()}.json`,
  fields: [
    {
      name: 'title',
      component: 'text',
      label: 'Titel',
    },
  ],
  data: form => ({
    title: form.title,
    modules: [],
  }),
});

export const CreatePostPlugin = new RemarkCreatorPlugin({
  label: 'Neuer Artikel',
  filename: form => `data/posts/${slugify(form.title).toLowerCase()}.md`,
  fields: [
    {
      name: 'title',
      component: 'text',
      label: 'Titel',
    },
  ],
  frontmatter: form => ({
    title: form.title,
    date: new Date(),
  }),
});
