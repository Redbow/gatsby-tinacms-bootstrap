import config from './auth_config.json';
import { isLocalhost } from './utils';

const makeAuthUrl = () => {
  const { domain: oauthLoginEndpoint, clientId } = config;
  const redirectUri = `http${isLocalhost() ? '' : 's'}://${window.location.host}/oauthCallback/`;
  const nonce = Math.random().toString(36).substring(7);

  const queryParams = `client_id=${clientId}&redirect_uri=${redirectUri}&scope=read_user+api&state=${nonce}&response_type=token`;

  return `${oauthLoginEndpoint}${queryParams}`;
};

const getUserProjects = async token => {
  const response = await fetch('https://gitlab.com/api/v4/projects?membership=true', {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await response.json();
  return data;
};

const verifyProject = async token => {
  const projects = await getUserProjects(token);
  return !!(projects.find(project => project.id === parseInt(config.projectId, 10)));
};

export const launchPopup = () => {
  // redirect to gitlab auth and receive the info from that tab/popup
  const popupHeight = 470;
  const popupWidth = 426;
  const left = window.screen.width / 2 - popupWidth / 2;
  const top = window.screen.height / 2 - popupHeight / 2;

  const popup = window.open(makeAuthUrl(), 'Gitlab Login',
    `menubar=no,location=no, resizable=no, scrollbars=no, status=no, width=${popupWidth}, height=${popupHeight}, top=${top}, left=${left}`);

  if (popup) popup.opener = window;
  return popup;
};

export const listenToMessageEvent = (resolve, reject) => {
  const windowEventHandler = event => {
    const hash = event.data;

    if ((/^react-devtools/gi).test(hash.source)) return;
    if (hash.type !== 'GITLAB_ACCESS_TOKEN') {
      console.error(hash.message);
      reject(hash.message);
    }

    const token = hash.access_token;
    verifyProject(token).then(hasAccess => {
      if (!hasAccess) {
        console.log('No project access.');
        reject('No project access.');
      }
      resolve(token);
    });
  };
  window.addEventListener('message', windowEventHandler, false);
};

export const processToken = (token, callbackFunction) => {
  if (!window.opener && token && typeof callbackFunction === 'function') {
    callbackFunction(token);
    return;
  }

  if (!token) {
    window.opener.postMessage(
      {
        type: 'error',
        message: 'No Access Token Found.',
      },
      window.location.origin,
    );
    window.close();
    return;
  }

  window.opener.postMessage(
    {
      type: 'GITLAB_ACCESS_TOKEN',
      access_token: token,
    },
    window.location.origin,
  );
  window.close();
};
