import React, { useContext } from 'react';
import { ActionButton } from 'tinacms';

import { GlobalStateContext } from '../context/GlobalContextProvider';
import config from './auth_config.json';

export default () => {
  const state = useContext(GlobalStateContext);

  return (
    <ActionButton
      onClick={async () => {
        fetch(`https://gitlab.com/api/v4/projects/${config.projectId}/pipeline?ref=master&variables[][key]=PUBLISH&variables[][value]=true`, {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${state.accessToken}`,
          },
        })
          .then(data => data.json())
          .then(console.log)
          .catch(console.log);
      }}
    >
      Publish
    </ActionButton>
  );
};
