import * as React from 'react';
import { ActionButton, useCMS } from 'tinacms';

export default ({ form }) => {
  const cms = useCMS();
  return (
    <ActionButton
      onClick={async () => {
        if (
          // eslint-disable-next-line
          !confirm( `Are you sure you want to delete ${form.values.jsonNode.fileRelativePath}?`)
        ) {
          return;
        }
        await cms.api.git.onDelete({
          relPath: form.values.jsonNode.fileRelativePath,
        });

        window.history.back();
      }}
    >
      Delete This Page
    </ActionButton>
  );
};
