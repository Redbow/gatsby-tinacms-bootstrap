export const isBrowser = () => typeof window !== 'undefined';

export const isLocalhost = () => isBrowser() && !!(window.location.href.match(/^http:\/\/localhost/));

export const isDev = () => process.env.NODE_ENV === 'development';

export const isProd = () => process.env.NODE_ENV === 'production';
