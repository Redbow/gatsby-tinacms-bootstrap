import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import { useGlobalJsonForm } from 'gatsby-tinacms-json';
import get from 'lodash.get';

import IconLink from '../partials/icon-link';

const GlobalContactDataForm = {
  label: 'Contact Data',
  fields: [
    {
      label: 'Name',
      name: 'rawJson.contactData.name',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Titel',
      name: 'rawJson.contactData.title',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Straße',
      name: 'rawJson.contactData.street',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Ort',
      name: 'rawJson.contactData.town',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Xing Profil URL',
      name: 'rawJson.contactData.xing',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'LinkedIn Profil URL',
      name: 'rawJson.contactData.linkedin',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Telefon',
      name: 'rawJson.contactData.phone',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Mobil',
      name: 'rawJson.contactData.mobile',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'E-Mail',
      name: 'rawJson.contactData.email',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
  ],
};

export const ContactForm = {
  label: 'Contact',
  name: 'contact',
  key: `contact-${Math.random().toString(36).substr(2, 9)}`,
  fields: [
    {
      label: 'Überschrift',
      description: 'Hier werden nur Überschrift und Bild gepflegt. Alle anderen Kontaktdaten kannst du in den allgemeinen Einstellungen ändern.',
      name: 'headline',
      component: 'text',
      default: '',
    },
    {
      label: 'Porträt',
      description: 'Mindestens 540px breit.',
      name: 'image',
      component: 'image',
      parse: filename => `../media/${filename}`,
      previewSrc: (formValues, { input }) => {
        const path = input.name.replace('rawJson', 'jsonNode');
        const gatsbyImageNode = get(formValues, path);
        if (!gatsbyImageNode) return '';
        // specific to gatsby-image
        return gatsbyImageNode.childImageSharp.fluid.src;
      },
      uploadDir: () => '/data/media/',
    },
  ],
};

export const contactData = graphql`
  fragment ContactData on PagesJsonModules {
    headline
    image {
      childImageSharp {
        fixed(width: 270) {
          ...GatsbyImageSharpFixed
        }
        fluid(maxWidth: 320) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;

export const Contact = ({ data }) => {
  const staticData = useStaticQuery(graphql`
    query ContactDataQuery {
      contactData: globalJson(fileRelativePath: { eq: "/data/global/contactData.json"}) {
        contactData {
          name
          email
          linkedin
          mobile
          phone
          street
          title
          town
          xing
        }

        ## Tina CMS Fields ##
        rawJson
        fileRelativePath
      }
    }
  `);

  const { contactData: staticContactData } = staticData.contactData;

  // TinaCMS Setup for global form
  useGlobalJsonForm(staticData.contactData, GlobalContactDataForm);

  return (
    <section className="container mx-auto flex flex-col flex-wrap items-start md:items-center md:flex-row my-24 md:my-16">
      <div className="flex-shrink px-4">
        { data.image
          ? <Img
            imgStyle={{
              width: 'auto',
              filter: 'grayscale(0.8) brightness(0.9)',
            }}
            fixed={data.image.childImageSharp.fixed} />
          : null }
      </div>
      <div className="flex-grow py-4 px-4">
        <h3 className="text-xl">{data.headline}</h3>
        <h2 className="text-3xl">{staticContactData.name}</h2>
        <h4 className="text-base italic pt-2 pb-4">{staticContactData.title}</h4>
        <p className="font-body pb-4">
          {staticContactData.street}<br/>
          {staticContactData.town}
        </p>
        <div>
          <IconLink icon="xing2" href={staticContactData.xing}></IconLink>
          <IconLink icon="linkedin2" href={staticContactData.linkedin}></IconLink>
        </div>
      </div>
      <div className="flex-grow px-4">
        <div className="flex flex-col">
          <IconLink icon="phone" href={`tel:${staticContactData.phone.replace(/\s|\(|\)/g, '')}`}>{staticContactData.phone}</IconLink>
          <IconLink icon="mobile" href={`tel:${staticContactData.mobile.replace(/\s|\(|\)/g, '')}`}>{staticContactData.mobile}</IconLink>
          <IconLink icon="envelop" href={`mailto:${staticContactData.email}`}>{staticContactData.email}</IconLink>
        </div>
      </div>
    </section>
  );
};
