import React from 'react';
import { graphql } from 'gatsby';
import get from 'lodash.get';

import Img from 'gatsby-image';

export const TextImageForm = {
  label: 'Text-Bild-Block',
  name: 'textImage',
  key: `textImage-${Math.random().toString(36).substr(2, 9)}`,
  fields: [
    {
      label: 'Bild links?',
      description: 'Soll das Bild auf großen Bildschirmen links oder rechts angezeigt werden?',
      name: 'isImageLeft',
      component: 'toggle',
      default: false,
    },
    {
      label: 'Überschrift',
      name: 'headline',
      component: 'text',
      default: '',
    },
    {
      label: 'Text',
      name: 'markdownCopy',
      component: 'markdown',
      default: '',
    },
    {
      label: 'Hintergrundbild',
      name: 'imageSide',
      component: 'image',
      parse: filename => `../media/${filename}`,
      previewSrc: (formValues, { input }) => {
        const path = input.name.replace('rawJson', 'jsonNode');
        const gatsbyImageNode = get(formValues, path);
        if (!gatsbyImageNode) return '';
        // specific to gatsby-image
        return gatsbyImageNode.childImageSharp.fluid.src;
      },
      uploadDir: () => '/data/media/',
    },
  ],
};

export const textImageData = graphql`
  fragment TextImageData on PagesJsonModules {
    headline
    imageSide {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    isImageLeft
  }
`;

export const TextImage = ({ data, markdown }) => (
  <section className={`flex flex-col-reverse container mx-auto py-8 ${data.isImageLeft ? 'md:flex-row-reverse' : 'md:flex-row'}`}>
    <div className="md:w-1/2 px-4">
      <h3 className="font-display">{data.headline}</h3>
      { markdown
        ? <div dangerouslySetInnerHTML={{ __html: markdown.html }} />
        : null }
    </div>
    <div className="w-full md:w-1/2 px-4 pb-4 md:pb-0">
      { data.imageSide
        ? <Img
          className="h-64"
          imgStyle={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            transform: 'scale(1.01)',
          }}
          fluid={data.imageSide.childImageSharp.fluid} /> : null }
    </div>
  </section>
);
