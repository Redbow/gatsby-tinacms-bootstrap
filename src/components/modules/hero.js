import React from 'react';
import { graphql } from 'gatsby';
import get from 'lodash.get';

import Img from 'gatsby-image';

export const HeroForm = {
  label: 'Hero',
  name: 'hero',
  key: `hero-${Math.random().toString(36).substr(2, 9)}`,
  fields: [
    {
      label: 'Überschriften',
      name: 'headlines',
      component: 'group-list',
      itemProps: item => ({
        label: item.text,
        key: item.id,
      }),
      defaultItem: () => ({
        id: Math.random()
          .toString(36)
          .substr(2, 9),
      }),
      fields: [
        {
          label: 'Überschrift',
          description: 'Jedes Element hier repräsentiert eine Zeile.',
          name: 'text',
          component: 'text',
        },
      ],
    },
    {
      label: 'Hintergrundbild',
      description: 'Mindestens 1920px breit.',
      name: 'backgroundImage',
      component: 'image',
      parse: filename => `../media/${filename}`,
      previewSrc: (formValues, { input }) => {
        const path = input.name.replace('rawJson', 'jsonNode');
        const gatsbyImageNode = get(formValues, path);
        if (!gatsbyImageNode) return '';
        // specific to gatsby-image
        return gatsbyImageNode.childImageSharp.fluid.src;
      },
      uploadDir: () => '/data/media/',
    },
  ],
};

export const heroData = graphql`
  fragment HeroData on PagesJsonModules {
    headlines {
      id
      text
    }
    backgroundImage {
      childImageSharp {
        fluid(maxWidth: 2400, sizes: "(max-aspect-ratio: 165/100) calc(100vh * 1.65 / 1), 100vw") {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;

export const Hero = ({ data }) => (
  <section className="relative h-screen-75 overflow-hidden">
    { data.backgroundImage
      ? <Img
        className="h-full w-full"
        imgStyle={{
          height: '100%',
          width: '100%',
          objectFit: 'cover',
        }}
        fluid={data.backgroundImage.childImageSharp.fluid} />
      : null }
    <div className="absolute container flex flex-col z-10 w-full center-transform top-0 left-0">
      { data.headlines
        ? <h2 className="font-bold uppercase px-4">{data.headlines.map(headline => <span key={headline.id}>{ headline.text }<br/></span>)}</h2>
        : null }
    </div>
  </section>
);
