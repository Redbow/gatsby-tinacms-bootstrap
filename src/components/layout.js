import React from 'react';
import { Helmet } from 'react-helmet';
import { withPlugin } from 'tinacms';

import Header from './partials/header';
import Footer from './partials/footer';
import AuthContainer from './auth-container';

import { isLocalhost, isDev, isProd } from '../utils/utils';
import { CreatePagePlugin, CreatePostPlugin } from '../utils/createContentPlugins';


const bodyClasses = {
  json: 'font-body antialiased',
  markdown: 'font-body antialiased',
};

function Layout({
  type,
  children,
  title,
}) {
  return (
    <div className={`layout layout--${type}`}>
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="author" content="Jana Deppe" />
        <meta name="description" content="Webentwicklung" />
        <title>{ title }</title>
        <link rel="canonical" href="http://www.janadeppe.de" />
        <html lang="de" />
        <body className={`${bodyClasses[type || 'json']}`} />
      </Helmet>
      <Header />
      { isDev() && !isLocalhost()
        ? <AuthContainer>
          <main> {children} </main>
        </AuthContainer>
        : null }
      { isProd() || isLocalhost()
        ? <main> {children} </main>
        : null }
      <Footer/>
    </div>
  );
}

export default withPlugin(Layout, [CreatePagePlugin, CreatePostPlugin]);
