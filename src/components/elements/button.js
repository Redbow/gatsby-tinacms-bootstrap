import React from 'react';

export default props => (
  <button
    type={props.type || 'button'}
    className={`
      transition bg-gray-800 hover:bg-gray-900 text-white font-bold py-2 px-4 rounded
      ${props.isLoading ? 'spinner' : ''}
    `}
    onClick={props.onClick}>
    {props.children}
  </button>
);
