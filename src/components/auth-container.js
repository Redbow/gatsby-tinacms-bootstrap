import React, { useEffect, useContext } from 'react';
import { navigate } from 'gatsby';

import { GlobalStateContext, GlobalDispatchContext } from '../context/GlobalContextProvider';

export default ({ children }) => {
  const state = useContext(GlobalStateContext);
  const dispatch = useContext(GlobalDispatchContext);

  useEffect(() => {
    if (state.accessToken) return;
    dispatch({ type: 'SET_REDIRECT_AFTER_LOGIN', payload: '/' });
    navigate('/login');
  });

  if (state.accessToken) {
    return children;
  }
  return (
    <div className="container mx-auto text-center">
      <h3 className="my-24 lg:my-48">Du wirst auf die Login-Seite weitergeleitet.</h3>
    </div>
  );
};
