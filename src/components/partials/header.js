import React, { useState } from 'react';
import { Link, useStaticQuery, graphql } from 'gatsby';
import { useGlobalJsonForm } from 'gatsby-tinacms-json';

const GlobalHeaderDataForm = {
  label: 'Header Data',
  fields: [
    {
      label: 'Site Title',
      name: 'rawJson.headerData.siteTitle',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Menu Items',
      name: 'rawJson.headerData.menuItems',
      component: 'group-list',
      itemProps: item => ({
        label: item.linkText,
        key: item.id,
      }),
      defaultItem: () => ({
        id: Math.random()
          .toString(36)
          .substr(2, 9),
      }),
      fields: [
        {
          label: 'Link Text',
          name: 'linkText',
          component: 'text',
        },
        {
          label: 'Path',
          description: 'Write your path, starting with a leading slash like so: "/about".',
          name: 'path',
          component: 'text',
        },
      ],
    },
  ],
};

export default () => {
  const staticData = useStaticQuery(graphql`
    query HeaderDataQuery {
      headerData: globalJson(fileRelativePath: { eq: "/data/global/headerData.json"}) {
        headerData {
          siteTitle
          menuItems {
            id
            linkText
            path
          }
        }

        ## Tina CMS Fields ##
        rawJson
        fileRelativePath
      }
    }
  `);

  // TinaCMS Setup for global form
  useGlobalJsonForm(staticData.headerData, GlobalHeaderDataForm);

  const { headerData } = staticData.headerData;
  const [menuState, setMenuState] = useState('hidden');

  function toggleMenuState() {
    setMenuState(menuState === 'hidden' ? 'block' : 'hidden');
  }

  return (
    <header className="container mx-auto px-4 text-center py-2 flex items-center">
      <Link className="flex-grow text-left text-black" to="/"> <h5>{headerData.siteTitle}</h5> </Link>
      <button className="uppercase text-gray-600 hover:text-gray-700 transition" onClick={toggleMenuState}>Menu</button>
      <nav className={`${menuState} fixed bg-white w-screen h-screen inset-0 z-50 flex flex-col`}>
        <button className="self-end p-4 text-gray-600 hover:text-gray-700 transition" onClick={() => setMenuState('hidden')}>X</button>
        <ul className="flex flex-col items-center justify-center h-full">
          {headerData.menuItems.map(item => (
            <li className="p-2 text-4xl uppercase" key={item.id}>
              <Link to={item.path}>{item.linkText}</Link>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
};
