import React from 'react';

export default ({ href, icon, children }) => (
  <a className="leading-10 pb-2" href={href} target="_blank" rel="noopener noreferrer">
    <span className={`icon-${icon} border-gray-600 border-solid border-2 p-2 mr-2 rounded-full`}></span>
    {children}
  </a>
);
