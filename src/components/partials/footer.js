import React from 'react';
import { Link } from 'gatsby';

export default () => (
  <footer className="container mx-auto text-center py-4">
    <Link to="/impressum">Impressum & Datenschutz</Link>
  </footer>
);
