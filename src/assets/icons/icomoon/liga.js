/* A polyfill for browsers that don't support ligatures. */
/* The script tag referring to this file must be placed before the ending body tag. */

/* To provide support for elements dynamically added, this script adds
   method 'icomoonLiga' to the window object. You can pass element references to this method.
*/
(function () {
  function supportsProperty(p) {
    const prefixes = ['Webkit', 'Moz', 'O', 'ms'];
    let i;
    const div = document.createElement('div');
    let ret = p in div.style;
    if (!ret) {
    // eslint-disable-next-line
      p = p.charAt(0).toUpperCase() + p.substr(1);
      for (i = 0; i < prefixes.length; i += 1) {
        ret = prefixes[i] + p in div.style;
        if (ret) {
          break;
        }
      }
    }
    return ret;
  }
  let icons;
  if (!supportsProperty('fontFeatureSettings')) {
    icons = {
      home: '&#xe900;',
      house: '&#xe900;',
      home3: '&#xe902;',
      house3: '&#xe902;',
      phone: '&#xe942;',
      telephone: '&#xe942;',
      envelop: '&#xe945;',
      mail: '&#xe945;',
      mobile: '&#xe958;',
      'cell-phone': '&#xe958;',
      mail2: '&#xea83;',
      contact2: '&#xea83;',
      linkedin: '&#xeac9;',
      brand64: '&#xeac9;',
      linkedin2: '&#xeaca;',
      brand65: '&#xeaca;',
      xing: '&#xead3;',
      brand74: '&#xead3;',
      xing2: '&#xead4;',
      brand75: '&#xead4;',
      0: 0,
    };
    delete icons['0'];
    window.icomoonLiga = function (els) {
      let classes;
      let el;
      let i;
      let innerHTML;
      let key;
      //   eslint-disable-next-line
      els = els || document.getElementsByTagName('*');
      if (!els.length) {
        //   eslint-disable-next-line
        els = [els];
      }
      for (i = 0; ; i += 1) {
        el = els[i];
        if (!el) {
          break;
        }
        classes = el.className;
        if (/icon-/.test(classes)) {
          innerHTML = el.innerHTML;
          if (innerHTML && innerHTML.length > 1) {
            //   eslint-disable-next-line
            for (key in icons) {
              //   eslint-disable-next-line
              if (icons.hasOwnProperty(key)) {
                innerHTML = innerHTML.replace(new RegExp(key, 'g'), icons[key]);
              }
            }
            el.innerHTML = innerHTML;
          }
        }
      }
    };
    window.icomoonLiga();
  }
}());
