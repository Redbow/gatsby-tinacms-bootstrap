import React from 'react';
import { graphql } from 'gatsby';
import { useLocalRemarkForm } from 'gatsby-tinacms-remark';

import Layout from '../components/layout';
import PublishButton from '../utils/PublishButton';

const RemarkPostForm = {
  label: 'Artikel',
  actions: [/* DeleteButton,  */PublishButton],
  fields: [
    {
      label: 'Titel',
      name: 'frontmatter.title',
      component: 'text',
      description: 'Wird oben im Browser-Tab angezeigt.',
    },
    {
      label: 'Inhalt',
      name: 'rawMarkdownBody',
      component: 'markdown',
    },
  ],
};

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        title
      }
      html

      ...TinaRemark
    }
  }
`;

function RemarkLayout({ data }) {
  const post = data.markdownRemark;

  // TinaCMS Setup
  useLocalRemarkForm(data.markdownRemark, RemarkPostForm);

  return (
    <Layout type="markdown">
      <article className="w-100 sm:w-5/6 md:w-3/4 lg:w-1/2 mx-auto pt-24 md:pt-48 px-4 max-w-xl">
        <h1 className="text-5xl mb-4">{post.frontmatter.title}</h1>
        <div className="markdown" dangerouslySetInnerHTML={{ __html: post.html }} />
      </article>
    </Layout>
  );
}

export default RemarkLayout;
