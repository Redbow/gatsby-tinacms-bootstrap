import React from 'react';
import { graphql } from 'gatsby';
import { useLocalJsonForm } from 'gatsby-tinacms-json';

import Layout from '../components/layout';
import PublishButton from '../utils/PublishButton';
import DeleteButton from '../utils/DeleteButton';

import { Hero, HeroForm } from '../components/modules/hero';
import { TextImage, TextImageForm } from '../components/modules/text-image';
import { Contact, ContactForm } from '../components/modules/contact';

const getModuleComponent = (key, data, markdown) => ({
  HeroForm: <Hero key={key} data={data} />,
  TextImageForm: <TextImage key={key} data={data} markdown={markdown}/>,
  ContactForm: <Contact key={key} data={data} />,
});

const JsonPageForm = {
  label: 'Seite',
  actions: [DeleteButton, PublishButton],
  fields: [
    {
      label: 'Titel',
      name: 'rawJson.title',
      component: 'text',
      description: 'Wird oben im Browser-Tab angezeigt.',
    },
    {
      label: 'Sektionen',
      name: 'rawJson.modules',
      component: 'blocks',
      templates: {
        HeroForm,
        TextImageForm,
        ContactForm,
      },
    },
  ],
};

export const query = graphql`
  query($slug: String!) {
    pagesJson(fields: { slug: { eq: $slug } }) {
      title
      modules {
        _template
        ...HeroData
        ...ContactData
        ...TextImageData
      }

      childrenPagesJsonBlockMarkdown {
        childMarkdownRemark {
          html
        }
      }

      ## Tina CMS Fields ##

      rawJson
      fileRelativePath
    }
  }
`;

function JsonLayout({ data }) {
  const post = data.pagesJson;

  // TinaCMS Setup
  useLocalJsonForm(data.pagesJson, JsonPageForm);

  return (
    <Layout title={post.title} type="json">
      {
        post.modules.map((module, index) => getModuleComponent(
          `${module._template}-${index}`,
          module,
          post.childrenPagesJsonBlockMarkdown[index].childMarkdownRemark,
        )[module._template])
      }
    </Layout>
  );
}

export default JsonLayout;
