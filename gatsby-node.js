const path = require('path');
const { createFilePath } = require('gatsby-source-filesystem');

exports.onCreateNode = ({ node, getNode, actions, createNodeId, createContentDigest }) => {
  const { createNode, createNodeField, createParentChildLink } = actions;

  // Add slug fields for page creation
  if (
    node.internal.type === 'MarkdownRemark'
    || node.internal.type === 'PagesJson'
  ) {
    const slug = createFilePath({ node, getNode, basePath: 'pages' });
    createNodeField({
      node,
      name: 'slug',
      value: slug,
    });
  }

  // Convert modules[i].copy into markdown nodes
  if (node.internal.type === 'PagesJson') {
    const markdownHost = {
      id: createNodeId(`${node.id} markdown host`),
      parent: node.id,
      internal: {
        contentDigest: createContentDigest(JSON.stringify(node.modules)),
        type: `${node.internal.type}MarkdownData`,
      },
    };

    createNode(markdownHost);

    createNodeField({
      node,
      name: 'markdownContent___NODE', // Before the ___NODE: Name of the new fields
      value: markdownHost.id, // Connects both nodes
    });

    node.modules.forEach((module, i) => {
      // eslint-disable-next-line no-param-reassign
      if (!module.markdownCopy) module.markdownCopy = '';

      const copyNode = {
        id: `${node.id} module ${i} markdown`,
        parent: markdownHost.id,
        internal: {
          content: module.markdownCopy,
          contentDigest: createContentDigest(module.markdownCopy),
          type: `${node.internal.type}BlockMarkdown`,
          mediaType: 'text/markdown',
        },
      };

      createNode(copyNode);

      createParentChildLink({ parent: node, child: copyNode });
    });
  }
};

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
      allPagesJson {
        nodes {
          fields {
            slug
          }
        }
      }
    }
  `);

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('./src/templates/markdown.js'),
      context: {
        slug: node.fields.slug,
      },
    });
  });
  result.data.allPagesJson.nodes.forEach(node => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('./src/templates/json.js'),
      context: {
        slug: node.fields.slug,
      },
    });
  });
};
